% identification
\ProvidesClass{resume}
\NeedsTeXFormat{LaTeX2e}
\LoadClass{article}


% packages
\RequirePackage{color}
\RequirePackage{xcolor}
\RequirePackage{hyperref}
\RequirePackage{geometry}
\RequirePackage{fontawesome5}
\RequirePackage{titlesec}
\RequirePackage[document]{ragged2e}


% settings
\pagestyle{empty}
\geometry{letterpaper,portrait,margin=1in}
\definecolor{darkblue}{RGB}{6,69,173}
\hypersetup{colorlinks=true, linkcolor=darkblue, urlcolor=darkblue}


% Contact commands
\newcommand{\ContactPhone}[2] {
    {\faPhone} \href{#1}{#2}
}

\newcommand{\ContactEmail}[2] {
    {\faEnvelope} \href{#1}{#2}
}

\newcommand{\ContactWeb}[2] {
    {\faGlobeAmericas} \href{#1}{#2}
}

\newcommand{\ContactLoc}[1] {
    {\faMapMarker*} #1
}


% Sections
\titleformat{\section}{\Large}{}{0em}{}[\titlerule]

\newcommand{\Title}[1]{{\large \textbf{#1}}}

\newcommand{\Location}[2]{{\large #1 -- #2}}

\newcommand{\Timeframe}[2]{\textcolor{darkgray}{\large {#1 -- #2}}}

\newcommand{\University}[5]
{
    \Title{#1} \\
    \vspace{0.2em}
    \Location{#2}{#3} \\
    \vspace{0.2em}
    \Timeframe{#4}{#5}
    \vspace{0.2em}
}

\newcommand{\Work}[5]
{
    \Title{#1} \\
    \vspace{0.2em}
    \Location{#2}{#3} \hfill \Timeframe{#4}{#5}
}